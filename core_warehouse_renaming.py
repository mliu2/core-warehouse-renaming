import os
import re

MODEL_PATH = "/home/menglinliu/Documents/data-warehouse-transforms/models"
PRODUCT = "gnpd"
WAREHOUSE_PATH = os.path.join(MODEL_PATH, "warehouse", "raw", PRODUCT)
HUB_PATH = os.path.join(WAREHOUSE_PATH, "hubs")
LINK_PATH = os.path.join(WAREHOUSE_PATH, "links")
SAT_PATH = os.path.join(WAREHOUSE_PATH, "sats")
STG_PATH = os.path.join(MODEL_PATH, "staging", "prep", PRODUCT)


def global_replace():
    path = os.path.join(MODEL_PATH, "*", "*", PRODUCT)
    sed_command_list = [
        f"find {path} -type f -name '*.yml' -o -name '*.sql'| xargs sed -i '/hk_gnpd_/!s/[^a-zA-Z]hk_/&gnpd_/g'",
        f"find {path} -type f -name '*.yml' -o -name '*.sql'| xargs sed -i '/bk_gnpd_/!s/[^a-zA-Z]bk_/&gnpd_/g'",
        f"find {path} -type f -name '*.yml' -o -name '*.sql'| xargs sed -i '/ref_gnpd_/!s/[^a-zA-Z]ref_/&gnpd_/g'",
        f"find {path} -type f -name '*.yml' | xargs sed -i 's/hk_gnpd_name:/hk_name:/g'",
        f"find {path} -type f -name '*.yml' -o -name '*.sql' | xargs sed -i 's/hk_gnpd_iso_country/hk_iso_country/g'",
        f"find {path} -type f -name '*.yml' -o -name '*.sql' | xargs sed -i 's/bk_gnpd_iso_country/bk_iso_country/g'"
    ]
    for sed_command in sed_command_list:
        print(sed_command)
        os.system(sed_command)


def generate_link_hk_map():
    stg_list = []
    link_list = []
    rename_list = []
    for file in filter(lambda x: x.endswith("sql") and x.startswith("link_gnpd"), os.listdir(LINK_PATH)):
        new_src_pk = file.replace("link_", "hk_").replace(".sql", "")
        with open(f"{LINK_PATH}/{file}", "r+") as f:
            data = f.read()
            old_src_pk = re.match(r'(.*)src_pk="(.*?)",',
                                  data, re.M | re.S).group(2)
            source_model_list = re.match(r'(.*)source_model=\[(.*?)"(.*)"(.*?)', data, re.M | re.S).group(
                3).replace("\n", "").replace(" ", "").replace("\"", "").split(",")
            for i in range(0, len(source_model_list)):
                link_list.append(file.replace(".sql", ""))
                stg_list.append(source_model_list[i])
                rename_list.append((old_src_pk, new_src_pk))
    return (stg_list, link_list, rename_list)


def link_hk_replace():
    (stg_list, link_list, rename_list) = generate_link_hk_map()
    for i in range(0, len(stg_list)):
        os.system(
            f"find {STG_PATH} -name '{stg_list[i]}*' | xargs sed -i s/{rename_list[i][0]}/{rename_list[i][1]}/g")
        os.system(
            f"find {WAREHOUSE_PATH} -name '*{link_list[i]}*' | xargs sed -i s/{rename_list[i][0]}/{rename_list[i][1]}/g")
            

if __name__ == "__main__":
    global_replace()
    link_hk_replace()
#rm -rf /home/menglinliu/test_data/models
#cp -r /home/menglinliu/Documents/data-warehouse-transforms/models /home/menglinliu/test_data
